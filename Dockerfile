FROM maven:3-amazoncorretto-21 AS builder

ADD ./pom.xml pom.xml
ADD ./src src/

RUN mvn clean package

From eclipse-temurin:21.0.4_7-jre

COPY --from=builder target/api-0.0.1-SNAPSHOT.jar api-0.0.1-SNAPSHOT.jar

EXPOSE 8081

CMD ["java", "-jar", "api-0.0.1-SNAPSHOT.jar"]