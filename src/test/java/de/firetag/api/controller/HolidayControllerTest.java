package de.firetag.api.controller;

import de.firetag.api.util.DateUtils;
import de.firetag.api.model.HolidayDTO;
import de.firetag.api.model.State;
import de.firetag.api.service.HolidayService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(HolidayController.class)
class HolidayControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HolidayService service;

    @Test
    public void shouldReturnHolidayForDayAndValidState() throws Exception {
        final LocalDate localDate = LocalDate.of(2023, Month.DECEMBER, 25);
        final String holidayName = "1. Weihnachtsfeiertag";
        final State state = State.NW;
        when(service.getHolidayByDayAndState(any(LocalDate.class), any())).thenReturn(getHoliday(localDate, holidayName));

        this.mockMvc.perform(get("/feiertage").param("day", DateUtils.getDateString(localDate)).param("state", state.name()))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", notNullValue()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.date", equalTo(DateUtils.getDateString(localDate))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", equalTo(holidayName)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.hint", nullValue()));
    }

    @Test
    public void shouldReturnHolidayForDay() throws Exception {
        final LocalDate localDate = LocalDate.of(2023, Month.DECEMBER, 25);
        final String holidayName = "1. Weihnachtsfeiertag";
        final String dateString = localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        when(service.getHolidayByDayAndState(any(LocalDate.class), any())).thenReturn(getHoliday(localDate, holidayName));

        this.mockMvc.perform(get("/feiertage").param("day", dateString))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", notNullValue()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.date", equalTo(dateString)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", equalTo(holidayName)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.hint", nullValue()));
    }

    @Test
    public void shouldReturnBadRequestForDayAndValidState() throws Exception {
        final LocalDate localDate = LocalDate.of(2023, Month.DECEMBER, 25);
        final String holidayName = "1. Weihnachtsfeiertag";
        final String dateString = localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        when(service.getHolidayByDayAndState(any(LocalDate.class), any())).thenReturn(getHoliday(localDate, holidayName));

        this.mockMvc.perform(get("/feiertage").param("day", dateString).param("state", "nonExistingState"))
                .andExpect(status().isBadRequest());
    }

    private HolidayDTO getHoliday(LocalDate localDate, String holidayName) {
        return buildHoliday(localDate, holidayName);
    }

    private HolidayDTO buildHoliday(LocalDate localDate, final String name) {
        final HolidayDTO holidayDTO = new HolidayDTO();
        holidayDTO.setDate(localDate);
        holidayDTO.setName(name);
        return holidayDTO;
    }
}