package de.firetag.api.client;

import de.firetag.api.exception.ForeignApiException;
import de.firetag.api.model.HolidayDTO;
import de.firetag.api.model.State;

import java.time.LocalDate;

public interface ForeignApiClient {
    public HolidayDTO fetchHoliday(LocalDate day, State state) throws ForeignApiException;
}
