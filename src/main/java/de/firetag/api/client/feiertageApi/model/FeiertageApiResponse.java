package de.firetag.api.client.feiertageApi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class FeiertageApiResponse {
    @JsonProperty("BW")
    private FeiertageApiBundeslandFeiertage BW;
    @JsonProperty("BY")
    private FeiertageApiBundeslandFeiertage BY;
    @JsonProperty("BE")
    private FeiertageApiBundeslandFeiertage BE;
    @JsonProperty("BB")
    private FeiertageApiBundeslandFeiertage BB;
    @JsonProperty("HB")
    private FeiertageApiBundeslandFeiertage HB;
    @JsonProperty("HH")
    private FeiertageApiBundeslandFeiertage HH;
    @JsonProperty("HE")
    private FeiertageApiBundeslandFeiertage HE;
    @JsonProperty("MV")
    private FeiertageApiBundeslandFeiertage MV;
    @JsonProperty("NI")
    private FeiertageApiBundeslandFeiertage NI;
    @JsonProperty("NW")
    private FeiertageApiBundeslandFeiertage NW;
    @JsonProperty("RP")
    private FeiertageApiBundeslandFeiertage RP;
    @JsonProperty("SL")
    private FeiertageApiBundeslandFeiertage SL;
    @JsonProperty("SN")
    private FeiertageApiBundeslandFeiertage SN;
    @JsonProperty("ST")
    private FeiertageApiBundeslandFeiertage ST;
    @JsonProperty("SH")
    private FeiertageApiBundeslandFeiertage SH;
    @JsonProperty("TH")
    private FeiertageApiBundeslandFeiertage TH;
    @JsonProperty("NATIONAL")
    private FeiertageApiBundeslandFeiertage NATIONAL;
}
