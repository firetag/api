package de.firetag.api.client.feiertageApi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.firetag.api.util.DateUtils;
import de.firetag.api.client.ForeignApiClient;
import de.firetag.api.client.feiertageApi.model.FeiertageApiBundeslandFeiertage;
import de.firetag.api.client.feiertageApi.model.FeiertageApiFeiertagData;
import de.firetag.api.client.feiertageApi.model.FeiertageApiResponse;
import de.firetag.api.exception.ForeignApiException;
import de.firetag.api.model.HolidayDTO;
import de.firetag.api.model.State;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;

@Service
public class FeiertageApiClient implements ForeignApiClient {

    private final static String BASE_URL = "https://feiertage-api.de/api/";
    private final HttpClient httpClient = HttpClient.newHttpClient();
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public HolidayDTO fetchHoliday(final LocalDate day, final State state) {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(buildUri(BASE_URL, day))
                .GET()
                .build();
        try {
            final HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            final String responseBody = response.body();
            if (responseBody == null) {
                throw new ForeignApiException();
            }
            return fetchHolidayFromResponse(responseBody, day, state);
        } catch (IOException | InterruptedException e) {
            throw new ForeignApiException();
        }
    }

    private HolidayDTO fetchHolidayFromResponse(final String responseBody, final LocalDate day, final State state)
            throws JsonProcessingException {
        final FeiertageApiResponse response = objectMapper.readValue(responseBody, FeiertageApiResponse.class);
        final FeiertageApiBundeslandFeiertage bundeslandFeiertage = fetchBundeslandFeiertage(response, state);
        Optional<Map.Entry<String, FeiertageApiFeiertagData>> gefundenerFeiertag;
        if (day == null) {
            gefundenerFeiertag = findNext(bundeslandFeiertage);
        } else {
            gefundenerFeiertag = findDated(day, bundeslandFeiertage);
        }
        return gefundenerFeiertag.map(feiertageApiFeiertagData -> {
            final HolidayDTO holidayDTO = new HolidayDTO();
            holidayDTO.setName(feiertageApiFeiertagData.getKey());
            holidayDTO.setDate(day != null ? day : LocalDate.parse(feiertageApiFeiertagData.getValue().getDatum()));
            holidayDTO.setHint(feiertageApiFeiertagData.getValue().getHinweis());
            return holidayDTO;
        }).orElse(null);
    }

    private Optional<Map.Entry<String, FeiertageApiFeiertagData>> findNext(@NonNull final FeiertageApiBundeslandFeiertage bundeslandFeiertage) {
        final LocalDate today = LocalDate.now();
        for (final Map.Entry<String, FeiertageApiFeiertagData> holidayNameHolidayData : bundeslandFeiertage.entrySet()) {
            final FeiertageApiFeiertagData feiertagData = holidayNameHolidayData.getValue();
            if (LocalDate.parse(feiertagData.getDatum()).isAfter(today)) {
                final Map.Entry<String, FeiertageApiFeiertagData> nameAndData = Map.of(holidayNameHolidayData.getKey(), feiertagData).entrySet().iterator().next();
                return Optional.of(nameAndData);
            }
        }
        return Optional.empty();
    }

    private Optional<Map.Entry<String, FeiertageApiFeiertagData>> findDated(@NonNull final LocalDate day, @NonNull final FeiertageApiBundeslandFeiertage bundeslandFeiertage) {
        for (final Map.Entry<String, FeiertageApiFeiertagData> holidayNameHolidayData : bundeslandFeiertage.entrySet()) {
            final FeiertageApiFeiertagData feiertagData = holidayNameHolidayData.getValue();
            if (feiertagData.getDatum().equalsIgnoreCase(DateUtils.getDateString(day))) {
                final Map.Entry<String, FeiertageApiFeiertagData> nameAndData = Map.of(holidayNameHolidayData.getKey(), feiertagData).entrySet().iterator().next();
                return Optional.of(nameAndData);
            }
        }
        return Optional.empty();
    }


    private FeiertageApiBundeslandFeiertage fetchBundeslandFeiertage(final FeiertageApiResponse response, final State state) {
        switch (state) {
            case BW -> {
                return response.getBW();
            }
            case BY -> {
                return response.getBY();
            }
            case BE -> {
                return response.getBE();
            }
            case BB -> {
                return response.getBB();
            }
            case HB -> {
                return response.getHB();
            }
            case HH -> {
                return response.getHH();
            }
            case HE -> {
                return response.getHE();
            }
            case MV -> {
                return response.getMV();
            }
            case NI -> {
                return response.getNI();
            }
            case NW -> {
                return response.getNW();
            }
            case RP -> {
                return response.getRP();
            }
            case SL -> {
                return response.getSL();
            }
            case SN -> {
                return response.getSN();
            }
            case ST -> {
                return response.getST();
            }
            case SH -> {
                return response.getSH();
            }
            case TH -> {
                return response.getTH();
            }
            case NONE -> {
                return response.getNATIONAL();
            }
            default -> throw new RuntimeException();
        }
    }

    private URI buildUri(String baseUrl, LocalDate day) {
        StringBuilder url = new StringBuilder()
                .append(baseUrl)
                .append("?jahr=").append(day == null ? LocalDate.now() : day.getYear());
        try {
            return new URI(url.toString());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}
