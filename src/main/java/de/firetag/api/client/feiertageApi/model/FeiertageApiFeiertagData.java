package de.firetag.api.client.feiertageApi.model;

import lombok.Data;

@Data
public class FeiertageApiFeiertagData {
    private String datum;
    private String hinweis;
}
