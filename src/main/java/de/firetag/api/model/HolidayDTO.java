package de.firetag.api.model;

import lombok.Data;

import java.time.LocalDate;

@Data
public class HolidayDTO {
    private String name;
    private LocalDate date;
    private String hint;
}
