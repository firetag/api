package de.firetag.api.model;

public enum State {
    BW,
    BY,
    BE,
    BB,
    HB,
    HH,
    HE,
    MV,
    NI,
    NW,
    RP,
    SL,
    SN,
    ST,
    SH,
    TH,
    NONE

}
