package de.firetag.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FiretagApi {

	public static void main(String[] args) {
		SpringApplication.run(FiretagApi.class, args);
	}

}
