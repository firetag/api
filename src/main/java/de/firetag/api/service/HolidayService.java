package de.firetag.api.service;

import de.firetag.api.client.ForeignApiClient;
import de.firetag.api.model.HolidayDTO;
import de.firetag.api.model.State;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
@RequiredArgsConstructor
@Service
public class HolidayService {

    private final ForeignApiClient apiClient;
    public HolidayDTO getHolidayByDayAndState(final LocalDate day, final State state) {
        return apiClient.fetchHoliday(day, state);
    }

    public HolidayDTO getNextHolidayState(final State state) {
        return apiClient.fetchHoliday(null, state);
    }
}
