package de.firetag.api.controller;

import de.firetag.api.model.HolidayDTO;
import de.firetag.api.model.State;
import de.firetag.api.service.HolidayService;
import jakarta.annotation.Nullable;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping("feiertage")
public class HolidayController {

    private final HolidayService holidayService;

    @GetMapping
    public HolidayDTO getHolidays(@RequestParam(required = false)  final Optional<LocalDate> day, @RequestParam(required = false) final State state) {
        if (day.isEmpty()) {
            return holidayService.getNextHolidayState(Optional.ofNullable(state).orElse(State.NONE));
        } else {
            return holidayService.getHolidayByDayAndState(day.orElse(null), Optional.ofNullable(state).orElse(State.NONE));
        }
    }

}
